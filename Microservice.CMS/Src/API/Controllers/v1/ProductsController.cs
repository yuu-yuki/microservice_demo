using System;
using MediatR;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

using Microservice.CMS.Application.UserCase.v1.Products.Queries.GetProductList;
using Microservice.CMS.Application.UserCase.v1.Products.Queries.GetProductDetail;

namespace Microservice.CMS.API.Controllers.v1
{
    [Route("api/v1/[controller]")]
    public class ProductsController : BaseController
    {
        [HttpGet]
        public async Task<IActionResult> GetAll() 
        {
            var vm = await Mediator.Send(new GetProductsListQuery());

            return base.Ok(vm);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id) 
        {
            var vm = await Mediator.Send(new GetProductDetailQuery{ Id = id });

            return base.Ok(vm);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] string name) 
        {
            return Ok("Create " + name);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Update(string id, [FromBody] string name) 
        {
            return NoContent();
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]   
        public async Task<IActionResult> Delete(string id) 
        {
            return NoContent();
        }
    }
}