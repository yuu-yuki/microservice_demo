using System.Collections.Generic;
using Microservice.CMS.Domain.Common;

namespace Microservice.CMS.Domain.Entities
{
    public class Product : AuditableEntity
    {
        public int Id {get; set;}
        public string Name {get; set;}
        public string? Description {get; set;}
        public decimal? Price {get; set;}
    }
}