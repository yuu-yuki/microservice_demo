using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

using Microservice.CMS.Application.Common.Interfaces.v1.Sender;
using Microservice.CMS.Domain.Entities;

namespace Microservice.CMS.Application.UserCase.v1.Products.Queries.GetProductDetail
{
    public class GetProductDetailQueryHandler : IRequestHandler<GetProductDetailQuery, ProductDetailVm> 
    {
        private List<ProductDetailVm> vm = new List<ProductDetailVm>();  
        private readonly IProductSender _productSender;

        public GetProductDetailQueryHandler(IProductSender productSender)
        {
            vm.Add( new ProductDetailVm { Id = 1, Name = "Test 1", Description = "Test 1 description", Price = 111});
            vm.Add( new ProductDetailVm { Id = 2, Name = "Test 2", Description = "Test 2 description", Price = 222});
            vm.Add( new ProductDetailVm { Id = 3, Name = "Test 3", Description = "Test 3 description", Price = 333});
            vm.Add( new ProductDetailVm { Id = 4, Name = "Test 4", Description = "Test 4 description", Price = 444});
        
            _productSender = productSender;
        }

        public async Task<ProductDetailVm> Handle(GetProductDetailQuery request, CancellationToken cancellationToken)
        {
            var product = vm.Select(x => new Product { Id = x.Id, Name = x.Name }).First();

            _productSender.SendProductDetail(product);

            return vm.SingleOrDefault(x => x.Id == request.Id);
        }
    }
}