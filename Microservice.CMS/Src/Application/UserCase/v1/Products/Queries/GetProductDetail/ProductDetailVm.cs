namespace Microservice.CMS.Application.UserCase.v1.Products.Queries.GetProductDetail
{
    public class ProductDetailVm 
    {
        public int Id {get; set;}
        public string Name {get; set;}
        public string Description {get; set;}
        public decimal? Price { get; set;}
    }
}