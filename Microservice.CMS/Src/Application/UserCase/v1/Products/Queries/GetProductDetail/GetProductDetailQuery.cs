using MediatR;

namespace Microservice.CMS.Application.UserCase.v1.Products.Queries.GetProductDetail
{
    public class GetProductDetailQuery : IRequest<ProductDetailVm>
    {
        public int Id {get; set;}
    }
}