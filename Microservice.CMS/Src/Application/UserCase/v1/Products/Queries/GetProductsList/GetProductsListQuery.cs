using MediatR;

namespace Microservice.CMS.Application.UserCase.v1.Products.Queries.GetProductList
{
    public class GetProductsListQuery : IRequest<ProductsListVm> 
    {
    }
}