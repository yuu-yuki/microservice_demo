using System.Collections.Generic;

namespace Microservice.CMS.Application.UserCase.v1.Products.Queries.GetProductList
{
    public class ProductsListVm 
    {
        public IList<ProductsDto> Products { get; set; } = new List<ProductsDto>();
    }
}