using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Microservice.CMS.Application.UserCase.v1.Products.Queries.GetProductList 
{
    public class GetProductsListQueryHandler : IRequestHandler<GetProductsListQuery, ProductsListVm> 
    {

        private ProductsListVm vm = new ProductsListVm();  

        public GetProductsListQueryHandler()
        {
            vm.Products.Add( new ProductsDto { Id = 1, Name = "Test 1", Description = "Test 1 description", Price = 111});
            vm.Products.Add( new ProductsDto { Id = 2, Name = "Test 2", Description = "Test 2 description", Price = 222});
            vm.Products.Add( new ProductsDto { Id = 3, Name = "Test 3", Description = "Test 3 description", Price = 333});
            vm.Products.Add( new ProductsDto { Id = 4, Name = "Test 4", Description = "Test 4 description", Price = 444});
        }
            

        public async Task<ProductsListVm> Handle(GetProductsListQuery request, CancellationToken cancellationToken)
        {
            return vm;
        }
    }
}