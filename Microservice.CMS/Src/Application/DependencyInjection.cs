using MediatR;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace Microservice.CMS.Application
{
    public static class DependencyInjection 
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services.AddMediatR(Assembly.GetExecutingAssembly());

            return services;
        }
    } 
}