using Microservice.CMS.Domain.Entities;

namespace Microservice.CMS.Application.Common.Interfaces.v1.Sender
{
    public interface IProductSender
    {
        void SendProductDetail(Product product);
    }
}