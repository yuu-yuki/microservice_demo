using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microservice.CMS.Messaging.Options.v1;
using Microservice.CMS.Application.Common.Interfaces.v1.Sender;
using Microservice.CMS.Messaging.Sender.v1.ProductSenders;

namespace Microservice.CMS.Messaging
{
    public static class DependencyInjection 
    {
        public static IServiceCollection AddMessageReceiverService(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<RabbitMqConfiguration>(configuration.GetSection("RabbitMq"));
            services.AddTransient<IProductSender, ProductSender>();
            return services;
        }
    }
}