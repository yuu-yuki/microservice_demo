using System.Text;
using RabbitMQ.Client;
using Newtonsoft.Json;
using Microsoft.Extensions.Options;
using Microservice.CMS.Application.Common.Interfaces.v1.Sender;
using Microservice.CMS.Messaging.Options.v1;
using Microservice.CMS.Domain.Entities;
using System;

namespace Microservice.CMS.Messaging.Sender.v1.ProductSenders
{
    public class ProductSender : IProductSender
    {
        private readonly string _hostname;
        private readonly string _queueName;
        private readonly string _username;
        private readonly string _password;
        private readonly int _port;

        public ProductSender(IOptions<RabbitMqConfiguration> rabbitMqOptions)
        {
            _hostname = rabbitMqOptions.Value.Hostname;
            _queueName = rabbitMqOptions.Value.QueueName;
            _username = rabbitMqOptions.Value.UserName;
            _password = rabbitMqOptions.Value.Password;
            _port = Int32.Parse(rabbitMqOptions.Value.Port);
        }

        public void SendProductDetail(Product product) {
            var factory = new ConnectionFactory() { 
                HostName = _hostname, 
                UserName = _username, 
                Password = _password,
                Port = _port
            };
        
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: _queueName, durable: false, exclusive: false, autoDelete: false, arguments: null);

                var json = JsonConvert.SerializeObject(product);
                var body = Encoding.UTF8.GetBytes(json);

                channel.BasicPublish(exchange: "", routingKey: _queueName, basicProperties: null, body: body);
            }
        }
    }
}